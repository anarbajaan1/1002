// Функции - блок кода
// Чтобы вызвать функцию мы пишем имя и круглые скобки
// раньше
// Процедура - блок кода просто выполняется 
// функция - блок кода который воплнется и возвращяет результат
// сейчас
// Одинаково
// function name(параметры){
// действия...
// }
// name(аргументы) - вызов функции
// DRY

// function hello(){
//     console.log('test hello')
// }

// hello() - вызов функции

// let a = hello;

// console.log(hello)
// console.log(a)

// a()

// let a = 5;
// let b = 10;

// function sum(a = 0, b = 0) {
//     console.log(a, b)
//     console.log(a + b)
//     return a + b;
// }

// let square = Math.pow(sum(3, 5), 2)

// console.log({ square })

// function square(a) {
//     return a * a;
// }

// let res = square(sum(3, 5,))

// console.log(res)

// function sumAll() {
//     const args = Array.from(arguments)
//     let sum = 0;
//     for(let digit of args){
//         sum += digit;
//     }
//     return sum;
// }

// let res = sumAll(1, 2, 3, 5)
// console.log({res})

// function chet(digit) {
//     if (digit % 2 === 0) {
//         return true;
//     } else {
//         return false;
//     }
//     // return digit % 2 === 0
// }

// function chetArr(arr = []) {
//     let res = [];

//     for (let i of arr) {
//         // console.log(i)
//         let isEven = chet(i)
//         if (isEven) {
//             res.push(i)
//         }
//         // isEven && res.push(i)
//     }
//     return res;
// }

// let nums = [1, 2, 3, 4, 5]

// console.log(chetArr(nums))

// function hello1(){
//     console.log('python')
// }

// hello1()

// 2 задание
// function hello2(name){

//     if(name === ''){
//         console.log('Привет, гость')
//     }else{
//         console.log('Привет,', name)
//     }
//     return name
// }

// hello2('')

// 3 задание
// function mul(n, m) {
//     console.log(n, m)
// }

// mul(2, 3)

// 4 задание
// function repeat(str, n=2){
//     return str.repeat(n)
// }

// console.log(repeat('asd'))

// 5 задание

// function rgb(a = 0, b = 0, c = 0) {
//     console.log(a, b, c)
//     return rgb
// }

// rgb()

// 6 задание

function avg(){
    const args = Array.from(arguments)
    let sum = 0;
    for(let digit of args){
        sum += digit;
    }
}